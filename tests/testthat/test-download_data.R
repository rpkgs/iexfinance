expect_sheet <- function(x, fiscal_quarter = 0, rows = 1) {
  expect_s3_class(x, "data.frame")
  expect_s3_class(x$report_date, "Date")
  expect_equal(x$fiscal_quarter, fiscal_quarter)
  expect_equal(nrow(x), rows)
}

test_that("download chart works", {
  con <- connect(Iex(), token = "Tpk_ce28e11cc96e4ced96210e4ae565f781", sandbox = TRUE)
  chart <- download_chart(con, symbol = "AAPL", from = "2022-01-01", to = "2022-02-01")
  expect_s3_class(chart, "data.frame")
  expect_named(chart, c('close', 'high', 'low', 'open', 'symbol', 'volume', 'id', 'key', 'subkey', 'date', 'updated', 'change_over_time', 'market_change_over_time', 'u_open', 'u_close', 'u_high', 'u_low', 'u_volume', 'f_open', 'f_close', 'f_high', 'f_low', 'f_volume', 'label', 'change', 'change_percent'))
  expect_equal(as.Date(range(chart$date)), as.Date(c("2022-01-03", "2022-02-01")))
})

test_that("download company works", {
  con <- connect(Iex(), token = "Tpk_ce28e11cc96e4ced96210e4ae565f781", sandbox = TRUE)
  company <- download_company(con, symbol = "AAPL")
  expect_s3_class(company, "data.frame")
  expect_true(all(c("symbol", "company_name") %in% names(company)))
  expect_equal(nrow(company), 1)
})

test_that("download sheets works", {
  con <- connect(Iex(), token = "Tpk_ce28e11cc96e4ced96210e4ae565f781", sandbox = TRUE)

  ignore_cols <- c("filing_type","currency","fiscal_date")

  balance <- download_balance(con, symbol = "AAPL", last = 1)
  FAI::expect_report_names(balance)
  FAI::expect_report_types(balance, ignore_cols)
  expect_sheet(balance)

  Sys.sleep(1)

  income <- download_income(con, symbol = "AAPL", last = 1)
  FAI::expect_report_names(income)
  FAI::expect_report_types(income, ignore_cols)
  expect_sheet(income)

  Sys.sleep(1)

  cashflow <- download_cashflow(con, symbol = "AAPL", last = 1)
  FAI::expect_report_names(cashflow)
  FAI::expect_report_types(cashflow, ignore_cols)
  expect_sheet(cashflow)
})
