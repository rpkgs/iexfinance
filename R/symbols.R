#' @inherit
#' FAI::download_symbols title description params details references return seealso
#' @export
setMethod("download_symbols", "IexConnection", function(drv, ...) {
  response <- httr::GET(
    sprintf("%s/ref-data/symbols", drv@uri),
    query = list(token = drv@token)
  )
  assertthat::assert_that(
    !httr::http_error(response),
    msg = {
      err <- httr::http_status(response)
      sprintf("%s (URI: %s)", err$message, gsub("token=[a-zA-Z0-9_]*", "token=YOUR_TOKEN", response$url))
    }
  )
  x <- httr::content(response, simplifyDataFrame = TRUE)
  names(x) <- snakecase::to_snake_case(names(x))
  x
})
