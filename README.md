
<!-- README.md is generated from README.Rmd. Please edit that file -->

# iexFinance

<!-- badges: start -->
<!-- badges: end -->

The goal of iexFinance is to make [iexcloud.io](https://iexcloud.io/)
accessible to R by using `FAI` functions.

See \[<https://rpkgs.gitlab.io/iexfinance/>\] for more information.

## Installation

You can install the development version of iexFinance like so:

``` r
remotes::install_gitlab("rpkgs/iexfinance")
```

## Example

Downloading financial data could be done by calling one of a
`download_*` function:

``` r
library(iexFinance)
con <- connect(iexFinance::Iex(), token = Sys.getenv("IEX_TOKEN_PK"))

download_chart(con, symbol = "AAPL", from = "2022-01-01", to = "2022-01-05") |> 
  dplyr::select(close, date)
#>      close       date
#> 117 182.01 2022-01-03
#> 118 179.70 2022-01-04
#> 119 174.92 2022-01-05
```

## Implemented API Endpoints

At the moment the following methods are implemented. If you miss some
function feel free to write an issue or make a PR.

-   `download_symbols` [Symbols](https://iexcloud.io/docs/api/#symbols)
-   `download_company` [Company](https://iexcloud.io/docs/api/#company)
-   `download_chart`
    [Chart](https://iexcloud.io/docs/api/#historical-prices)
-   `download_balance` [Balance
    Sheet](https://iexcloud.io/docs/api/#balance-sheet)
-   `download_cashflow` [Cash
    Flow](https://iexcloud.io/docs/api/#cash-flow)
-   `download_income` [Income
    Statement](https://iexcloud.io/docs/api/#income-statement)
